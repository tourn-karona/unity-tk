using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;

static public class UnityExtension
{
	static public void EnumerateChildRecursively (this Component parent, UnityAction<Transform> callback)
	{
		foreach (Transform child in parent.transform)
		{
			if (callback != null) { callback (child); }
			EnumerateChildRecursively (child, callback);
		}
	}

	static public void EnumerateChildRecursivelyOfType<T> (this Component parent, UnityAction<T> callback) where T : Component
	{
		foreach (Transform child in parent.transform)
		{
			if (callback != null)
			{
				T com = child.GetComponent<T> ();
				if (com) { callback (com); }
			}
			EnumerateChildRecursivelyOfType<T> (child, callback);
		}
	}

	static public IList<T> FindChildrenOfType<T> (this Component parent) where T : Component
	{
		IList<T> list = new List<T> ();
		parent.EnumerateChildRecursivelyOfType<T> (child => list.Add (child));
		return list;
	}

	static public float LocalPositionX(this Transform target)
	{
		return target.localPosition.x;
	}

	static public float LocalPositionY (this Transform target)
	{
		return target.localPosition.y;
	}

	static public float LocalPositionZ (this Transform target)
	{
		return target.localPosition.z;
	}

	static public void LocalPositionX (this Transform target, float x)
	{
		Vector3 pos = target.localPosition;
		pos.x = x;
		target.localPosition = pos;
	}

	static public void LocalPositionY (this Transform target, float y)
	{
		Vector3 pos = target.localPosition;
		pos.y = y;
		target.localPosition = pos;
	}

	static public void LocalPositionZ (this Transform target, float z)
	{
		Vector3 pos = target.localPosition;
		pos.z = z;
		target.localPosition = pos;
	}

	static public float PositionX (this Transform target)
	{
		return target.localPosition.x;
	}

	static public float PositionY (this Transform target)
	{
		return target.localPosition.y;
	}

	static public float PositionZ (this Transform target)
	{
		return target.localPosition.z;
	}

	static public void PositionY (this Transform target, float y)
	{
		Vector3 pos = target.position;
		pos.y = y;
		target.position = pos;
	}

	static public void PositionX (this Transform target, float x)
	{
		Vector3 pos = target.position;
		pos.x = x;
		target.position = pos;
	}

	static public void PositionZ (this Transform target, float z)
	{
		Vector3 pos = target.position;
		pos.z = z;
		target.position = pos;
	}

	static public void LocalScaleY (this RectTransform target, float y)
	{
		Vector3 scale = target.localScale;
		scale.y = y;
		target.localScale = scale;
	}

	/// <summary>
	/// Return list of children
	/// </summary>
	/// <param name="parent"></param>
	/// <returns></returns>
	static public Transform[] GetChildren (this Transform parent)
	{
		// Count of children
		int count = parent.childCount;

		Transform[] children = new Transform[count];

		for (int i = 0; i < count; i++)
		{
			children[i] = parent.GetChild (i);
		}

		return children;
	}

	static public Texture2D CompressDXT1 (this Texture2D texture)
	{
		var clone = new Texture2D (texture.width, texture.height, TextureFormat.RGB24, false);
		clone.SetPixels (texture.GetPixels ());
		clone.Apply ();
		return clone;
	}

	// Note that Color and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
	static public string ColorToHex (this Color color )
	{
		int r = (int)(color.r * 255);
		int g = (int)(color.g * 255);
		int b = (int)(color.b * 255);
		int a = (int)(color.a * 255);
		string hex = "#" + r.ToString ("X2") + g.ToString ("X2") + b.ToString ("X2") + a.ToString ("X2");
		return hex;
	}

	static public Color HexToColor ( this string hex )
	{
		if ( string.IsNullOrEmpty ( hex ) )
		{
			throw new System.InvalidOperationException ( "Argument cannot be empty or null." );
		}

		int startIndex = 0;
		byte r = 0, g = 0, b = 0, a = 255;

		while ( hex[startIndex] == '#' )
		{
			startIndex++;
		}

		r = byte.Parse (new string(new char[]{ hex[startIndex], hex[startIndex + 1] }), System.Globalization.NumberStyles.HexNumber);
		startIndex += 2;
		g = byte.Parse (new string(new char[]{ hex[startIndex], hex[startIndex + 1] }), System.Globalization.NumberStyles.HexNumber);
		startIndex += 2;
		b = byte.Parse (new string(new char[]{ hex[startIndex], hex[startIndex + 1] }), System.Globalization.NumberStyles.HexNumber);
		startIndex += 2;
		if ( startIndex < hex.Length )
		{
			a = byte.Parse ( new string ( new char[] { hex[startIndex], hex[startIndex + 1] } ), System.Globalization.NumberStyles.HexNumber );
		}

		return new Color32 ( r, g, b, a );
	}

	static public bool TrySetEnabled<T> ( this GameObject gameObject, bool enable ) where T : MonoBehaviour
	{
		T com = gameObject.GetComponent<T>();

		if ( com == null )
		{
			return false;
		}

		com.enabled = enable;

		return true;
	}

	static public bool TryEnable<T> (this GameObject gameObject) where T : MonoBehaviour
	{
		return gameObject.TrySetEnabled<T> ( true );
	}

	static public bool TryDisable<T> ( this GameObject gameObject ) where T : MonoBehaviour
	{
		return gameObject.TrySetEnabled<T> ( false );
	}

	static public Canvas GetCanvas (this GameObject gameObject)
	{
		Canvas com = gameObject.GetComponent<Canvas>();
		return com;
	}

	static public CanvasGroup GetCanvasGroup ( this GameObject gameObject )
	{
		CanvasGroup com = gameObject.GetComponent<CanvasGroup>();
		return com;
	}

	static public Button GetButton ( this GameObject gameObject )
	{
		Button com = gameObject.GetComponent<Button>();
		return com;
	}

	static public Text GetText ( this GameObject gameObject )
	{
		Text com = gameObject.GetComponent<Text>();
		return com;
	}

	static public InputField GetInputField ( this GameObject gameObject )
	{
		InputField com = gameObject.GetComponent<InputField>();
		return com;
	}

	static public ScrollRect GetScrollRect ( this GameObject gameObject )
	{
		ScrollRect com = gameObject.GetComponent<ScrollRect>();
		return com;
	}

	static public Toggle GetToggle ( this GameObject gameObject )
	{
		Toggle com = gameObject.GetComponent<Toggle>();
		return com;
	}

	static public ToggleGroup GetToggleGroup ( this GameObject gameObject )
	{
		ToggleGroup com = gameObject.GetComponent<ToggleGroup>();
		return com;
	}

	static public Image GetImage ( this GameObject gameObject )
	{
		Image com = gameObject.GetComponent<Image>();
		return com;
	}

	static public Dropdown GetDropdown ( this GameObject gameObject )
	{
		Dropdown com = gameObject.GetComponent<Dropdown>();
		return com;
	}

	static public Slider GetSlider ( this GameObject gameObject )
	{
		Slider com = gameObject.GetComponent<Slider>();
		return com;
	}

	static public RawImage GetRawImage ( this GameObject gameObject )
	{
		RawImage com = gameObject.GetComponent<RawImage>();
		return com;
	}

	static public Texture2D ToTexture (this Sprite sprite)
	{
		var croppedTexture = new Texture2D ((int)sprite.rect.width, (int)sprite.rect.height);
		var pixels = sprite.texture.GetPixels ((int)sprite.textureRect.x,
												(int)sprite.textureRect.y,
												(int)sprite.textureRect.width,
												(int)sprite.textureRect.height);
		croppedTexture.SetPixels (pixels);
		croppedTexture.Apply ();
		return croppedTexture;
	}

	static public Sprite ToSprite ( this Texture2D texture )
	{
		return texture.ToSprite ( new Vector2 (0.5f, 0.5f) );
	}

	static public Sprite ToSprite ( this Texture2D texture, Vector2 pivot )
	{
		Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), pivot);
		return sprite;
	}

	static public void SetAlpha (this MaskableGraphic g, float a)
	{
		var color = g.color;
		color.a = a;
		g.color = color;
	}

	static public bool IsEmpty (this InputField input)
	{
		return string.IsNullOrEmpty (input.text);
	}

	static public bool IsEmpty (this Text text)
	{
		return string.IsNullOrEmpty (text.text);
	}

	static public bool IsEmpty ( this Dropdown dropdown )
	{
		return dropdown.options.Count == 0;
	}

	static public void SetText (this Text text, object value)
	{
		text.text = value.ToString ();
	}

	static public void SetText ( this InputField input, object value )
	{
		input.text = value.ToString ();
	}

	static public void Clear (this Text text)
	{
		text.text = string.Empty;
	}

	static public void Clear ( this InputField input )
	{
		input.text = string.Empty;
	}

	static public void AnchoredPositionX (this RectTransform target, float x)
	{
		Vector2 position = target.anchoredPosition;
		position.x = x;
		target.anchoredPosition = position;
	}

	static public void AnchoredPositionY (this RectTransform target, float y)
	{
		Vector2 position = target.anchoredPosition;
		position.y = y;
		target.anchoredPosition = position;
	}

	private static void SetAlpha<T> (T graphic, float alpha) where T : MaskableGraphic
	{
		Color color = graphic.color;
		color.a = alpha;
		graphic.color = color;
	}

	static public void SetAlpha (this Image image, float alpha)
	{
		SetAlpha<Image> (image, alpha);
	}

	static public void SetAlpha (this Text text, float alpha)
	{
		SetAlpha<Text> (text, alpha);
	}

	static public void SetAlpha (this RawImage image, float alpha)
	{
		SetAlpha<RawImage> (image, alpha);
	}
}
