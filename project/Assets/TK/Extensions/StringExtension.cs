using System.Text.RegularExpressions;
using System.Text;
using UnityEngine;

static public class StringExtension
{
	static public string Repeat ( this string value, int count )
	{
		if ( count < 1 )
		{
			return value;
		}

		StringBuilder stringBuilder = new StringBuilder(value);

		while ( count > 0 )
		{
			stringBuilder.Append ( value );
			count--;
		}

		return stringBuilder.ToString ();
	}

	static public string WithSpace (this string value)
	{
		return Regex.Replace (value, "[A-Z]", " $0");
	}

	static public bool IsNullOrEmpty (this string value)
	{
		return string.IsNullOrEmpty (value);
	}

	static public string ClampString (this string value, int max, string suffix = "")
	{
		int length = value.Length;
		if ( length > max )
		{
			string result = value.Substring (0, max);
			if ( !string.IsNullOrEmpty ( suffix ) )
			{
				result += suffix;
			}
			return result;
		}
		else
		{
			return value;
		}
	}

	static public string UppercaseFirst ( this string s )
	{
		// Check for empty string.
		if ( string.IsNullOrEmpty ( s ) )
		{
			return string.Empty;
		}
		// Return char and concat substring.
		return char.ToUpper ( s[0] ) + s.Substring ( 1 );
	}

	#region Rich Text

	static public string FormatSize ( this string value, int size )
	{
		return string.Format ( "<size={0}>{1}</size>", size, value );
	}

	static public string FormatColor ( this string value, Color color )
	{
		return string.Format ( "<color={0}>{1}</color>", color.ColorToHex (), value );
	}

	static public string FormatBold ( this string value )
	{
		return string.Format ( "<b>{0}</b>", value );
	}

	static public string FormatItalic ( this string value )
	{
		return string.Format ( "<i>{0}</i>", value );
	}

	#endregion
}
