using UnityEngine;
using System;
using System.Collections.Generic;

namespace TK
{
    public class SelectORMMaker : SQLMaker
    {
        private class TableInfo
        {
            public Type type;
            public string tableName;
            public Type parentType;

            public TableInfo (Type type, string tableName, Type parentType = null)
            {
                this.type = type;
                this.tableName = tableName;
                this.parentType = parentType;
            }
        }

        private List<TableInfo> _tableInfos;
        private List<string> _selects;
        private List<string> _tables;
        private List<string> _wheres;
        private List<string> _joins;
        private List<string> _joinsQuerys;
        private Type _type;
        private SqliteJoinType _joinType;
        private string _tableName;

        /// <summary>
        /// Gets the table type.
        /// </summary>
        /// <value>The type.</value>
        public Type Type {
            get {
                return _type;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TK.SelectORMMaker"/> class.
        /// </summary>
        /// <param name="type">Type.</param>
        /// <param name="tableName">Table name.</param>
        public SelectORMMaker (Type type, string tableName)
        {
            _type = type;
            _tableName = tableName;
            if (_tableName == "") {
                _tableName = Query.GetTableName (type);
            }

            _selects = new List<string> ();
            _tables = new List<string> ();
            _wheres = new List<string> ();
            _joins = new List<string> ();
            _joinsQuerys = new List<string> ();

            _tableInfos = new List<TableInfo> ();
            _tableInfos.Add (new TableInfo (this._type, this._tableName));
            _tables.Add (this._tableName);
        }

        /// <summary>
        /// Sets the type.
        /// </summary>
        /// <returns>The type.</returns>
        /// <param name="type">Type.</param>
        /// <param name="index">Index.</param>
        public string SetType (Type type, Type parent = null)
        {
            if (type == null) {
                _type = type;
                return "";
            }

            _type = type;
            _tableName = Query.GetTableName (type);

            if (_tableName != "") {
                if (!_tables.Contains (_tableName)) {
                    _tableInfos.Add (new TableInfo (_type, _tableName, parent));
                    _tables.Add (_tableName);
                }
            }

            return _tableName;
        }

        /// <summary>
        /// Sets the select.
        /// </summary>
        /// <param name="fieldName">Field name.</param>
        public void SetSelect (string fieldName)
        {
            _selects.Add (GenerateSelectAliasSQL (_tableName, fieldName));
        }

        /// <summary>
        /// Sets the join.
        /// </summary>
        /// <param name="fieldName">Field name.</param>
        /// <param name="joinTable">Join table.</param>
        /// <param name="joinFieldName">Join field name.</param>
        public void SetJoin (Type tableType, string fieldName, string joinFieldName)
        {
            string tableName = Query.GetTableName (tableType);
            string joinQuery = GenerateJoinSQL (tableName, _tableName, fieldName, joinFieldName, _joinType);

            if (joinQuery == "") {
                string key = Period (tableName, joinFieldName);
                string value = Period (_tableName, fieldName);
                _wheres.Add (EqualsSign (key, value));
            } else {
                _joins.Add (_tableName);
                _joinsQuerys.Add (joinQuery);
            }
        }

        /// <summary>
        /// Sets the type of the join.
        /// </summary>
        /// <param name="joinType">Join type.</param>
        public void SetJoinType (SqliteJoinType joinType = SqliteJoinType.None)
        {
            this._joinType = joinType;
        }

        /// <summary>
        /// Execute this instance.
        /// </summary>
        public string GetQuery (string addQuery = "")
        {
            if (_joins.Count > 0) {
                foreach (string j in _joins) {
                    if (_tables.Contains (j)) {
                        _tables.Remove (j);
                    }
                }
            }

            return GenerateSelectSQL (_selects, _tables, _joinsQuerys, _wheres, addQuery);
        }
    }

    public partial class Query
    {
        protected System.Reflection.BindingFlags _blindingFlags = System.Reflection.BindingFlags.NonPublic;

        /// <summary>
        /// Sets the blinding flags.
        /// Only BindingFlags.NonPublic and BindingFlags.Public
        /// </summary>
        /// <value>The blinding flags.</value>
        public System.Reflection.BindingFlags BlindingFlags {
            set {
                if (value == System.Reflection.BindingFlags.NonPublic || value == System.Reflection.BindingFlags.Public) {
                    _blindingFlags = value;
                } else {
                    Debug.LogWarning ("Only BindingFlags.NonPublic and BindingFlags.Public");
                }
            }
        }

        /// <summary>
        /// Cleans the useless.
        /// </summary>
        /// <returns>The useless.</returns>
        /// <param name="data">Data.</param>
        protected Dictionary<string, object> CleanUseless (Dictionary<string, object> data)
        {
            Dictionary<string, object> temp = new Dictionary<string, object> ();
            foreach (KeyValuePair<string, object> pair in data) {
                if (pair.Value != null) {
                    if (ScriptUtility.IsValueType (pair.Value.GetType ())) {
                        temp.Add (pair.Key, pair.Value);
                    }
                }
            }

            return temp;
        }

        /// <summary>
        /// INSERT the specified tableName and t.
        /// </summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="t">T.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        protected void INSERT<T> (string tableName, T t)
        {
            Dictionary<string, object> data = Reflection.Convert<T> (t, null, _blindingFlags);
            data = CleanUseless (data);

            INSERT (tableName, data);
        }

        /// <summary>
        /// INSERT the specified t.
        /// </summary>
        /// <param name="t">T.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void INSERT<T> (T t)
        {
            INSERT<T> (GetTableName<T> (), t);
        }

        /// <summary>
        /// INSERT BATCH.
        /// </summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="list">List.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        protected void INSERT_BATCH<T> (string tableName, List<T> list)
        {
            List<Dictionary<string, object>> data = Reflection.Convert<T> (list, _blindingFlags);

            string[] columns = null;
            string[][] qData = new string[data.Count][];
            for (int i = 0; i < data.Count; i++) {
                data [i] = CleanUseless (data [i]);

                string[] temp = new string[data [i].Count];
                int index = 0;
                foreach (KeyValuePair<string, object> pair in data [i]) {
                    if (i == 0) {
                        if (columns == null) {
                            columns = new string[data [i].Count];
                        }
                        columns [index] = pair.Key;
                    }

                    temp [index] = pair.Value.ToString ();
                    index++;
                }

                qData [i] = temp;
            }

            INSERT_BATCH (tableName, columns, qData);
        }

        /// <summary>
        /// INSERT BATCH.
        /// </summary>
        /// <param name="list">List.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void INSERT_BATCH<T> (List<T> list)
        {
            INSERT_BATCH<T> (GetTableName<T> (), list);
        }

        /// <summary>
        /// INSERT BATCH.
        /// </summary>
        /// <param name="list">List.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void INSERT_BATCH<T> (T[] list)
        {
            INSERT_BATCH<T> (GetTableName<T> (), new List<T> (list));
        }

        /// <summary>
        /// UPDATE the specified t and addQuery.
        /// </summary>
        /// <param name="t">T.</param>
        /// <param name="addQuery">Add query.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void UPDATE<T> (T t, string addQuery)
        {
            Dictionary<string, object> data = Reflection.Convert<T> (t, null, _blindingFlags);
            data = CleanUseless (data);

            UPDATE (GetTableName<T> (), data, addQuery);
        }

        /// <summary>
        /// UPDATE the specified t, key and value.
        /// </summary>
        /// <param name="t">T.</param>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void UPDATE<T> (T t, string key, object value)
        {
            UPDATE<T> (t, GenerateWhereKeyValueSQL (key, value));
        }

        /// <summary>
        /// SELECT the specified tableName and addQuery.
        /// </summary>
        /// <param name="tableName">Table name.</param>
        /// <param name="addQuery">Add query.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        protected T[] SELECT<T> (string tableName, string addQuery)
        {
            if (tableName == "") {
                return null;
            }

            SelectORMMaker mapper = new SelectORMMaker (typeof(T), tableName);
            mapper = Reflection.SetSelectORMMaker (mapper, _blindingFlags);

            DataTable data = ExecuteQuery (mapper.GetQuery (addQuery));
            if (data == null || data.Rows.Count <= 0) {
                return null;
            }

            return Reflection.Convert<T> (data, _blindingFlags);
        }

        /// <summary>
        /// SELECT the specified addQuery.
        /// </summary>
        /// <param name="addQuery">Add query.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public T[] SELECT<T> (string addQuery = "")
        {
            return SELECT<T> (GetTableName<T> (), addQuery);
        }

        /// <summary>
        /// SELECT the specified key and value.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public T[] SELECT<T> (string key, object value)
        {
            return SELECT<T> (GetTableName<T> (), GenerateWhereKeyValueSQL (key, value));
        }

        /// <summary>
        /// DELETE the specified addQuery.
        /// </summary>
        /// <param name="addQuery">Add query.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void DELETE<T> (string addQuery = "")
        {
            DELETE (GetTableName<T> (), addQuery);
        }

        /// <summary>
        /// DELETE the specified key and value.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void DELETE<T> (string key, object value)
        {
            DELETE (GetTableName<T> (), key, value);
        }

        /// <summary>
        /// Gets the name of the table.
        /// </summary>
        /// <returns>The table name.</returns>
        /// <param name="type">Type.</param>
        public static string GetTableName (Type type)
        {
            TableAttribute table = type.GetAttributeValue<TableAttribute> ();

            if (table == null) {
                Debug.LogError ("Not set the table attribute.");
                return "";
            } else if (table.TableName == "") {
                return type.Name;
            } else {
                return table.TableName;
            }
        }

        /// <summary>
        /// Gets the name of the table.
        /// </summary>
        /// <returns>The table name.</returns>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public static string GetTableName<T> ()
        {
            return GetTableName (typeof(T));
        }
    }
}
