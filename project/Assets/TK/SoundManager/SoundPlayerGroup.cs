using System.Collections.Generic;
using UnityEngine;

namespace TK.SoundManagement
{
	public class SoundPlayerGroup
	{
		private Transform _parent = null;
		private Queue<SoundPlayer> _freePlayers = new Queue<SoundPlayer> ();
		private List<SoundPlayer> _busyPlayers = new List<SoundPlayer> ();
		private float _volume = 0;
		private object _lock = new object();
		private int _freePlayerCount = 0;
		private int _busyPlayerCount = 0;
		private int _i = 0;
		private SoundPlayer _player = null;

		public float Volume
		{
			get { return _volume; }
			set { _volume = Mathf.Clamp01 ( value ); }
		}

		public SoundPlayerGroup ( Transform parent )
		{
			_parent = parent;
		}

		public SoundPlayer Play ( SoundData data, float fadeTime, bool loop, float volume )
		{
			lock ( _lock )
			{
				SoundPlayer player = null;

				if ( _freePlayerCount > 0 )
				{
					player = _freePlayers.Dequeue ();
					_freePlayerCount--;
					player.gameObject.SetActive ( true );
				}
				else
				{
					player = SoundPlayer.Create ( _parent );
				}

				player.Play ( data.name, data.clip, fadeTime, volume, loop );
				_busyPlayers.Add ( player );
				_busyPlayerCount++;

				return player;
			}
		}

		public void Stop ( float fadeTime = 0f )
		{
			lock ( _lock )
			{
				for ( int i = 0; i < _busyPlayerCount; i++ )
				{
					_busyPlayers[i].Stop ( fadeTime );
				}
			}
		}

		public bool IsPlaying ( string name )
		{
			return _busyPlayers.Exists ( s => s.KeyName == name && s.IsPlaying );
		}

		public void Update ()
		{
			lock ( _lock )
			{
				for ( _i = _busyPlayerCount - 1; _i >= 0; _i-- )
				{
					_player = _busyPlayers[_i];

					if ( _player.IsPlaying == false )
					{
						_busyPlayers.RemoveAt ( _i );
						_busyPlayerCount--;
						_player.gameObject.SetActive ( false );
						_freePlayers.Enqueue ( _player );
						_freePlayerCount++;
						continue;
					}

					_player.ExecuteUpdate ( _volume );
				}
			}
		}
	}
}
