﻿using UnityEditor;
using System.Collections;

namespace TK.Editor
{
    public class EditorCoroutine
    {
        protected IEnumerator _coroutine;

        public EditorCoroutine (IEnumerator coroutine)
        {
            this._coroutine = coroutine;
        }

        public void Start ()
        {
            EditorApplication.update += Update;
        }

        protected void Update ()
        {
            Stop ();
        }

        protected void Stop ()
        {
            if (!_coroutine.MoveNext ()) {
                EditorApplication.update -= Update;
            }
        }
    }
}
