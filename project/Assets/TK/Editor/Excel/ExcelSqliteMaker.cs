﻿using UnityEditor;
using UnityEngine;
using System;
using System.IO;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using NPOI.SS.UserModel;

namespace TK.Editor
{
	public class ExcelSqliteMaker
	{
		protected List<ISheet> _listSheet;
		protected ExcelImportMaker _maker;
		protected Query _query;
		protected StringBuilder _stringBuilder;
		protected string _excelFilePath;
		protected string _dbPath;
		protected string _dbName;
		protected string _path;
		protected int _index;
		protected bool _splitFlag;

		public ExcelSqliteMaker ( string excelFilePath, string dbPath, string dbName )
		{
			_excelFilePath = excelFilePath;
			_dbPath = dbPath;
			_dbName = dbName;
		}

		protected IEnumerator CreateTable ()
		{
			EditorUtility.DisplayProgressBar ( "[Exce -> Sqlite DB] Converter",
											  string.Format ( "{0}({1}/{2})",
															 _listSheet[_index].SheetName,
															 _index,
															 _listSheet.Count ),
											  (float)_index / (float)_listSheet.Count );

			Action innerCreateTable = () =>
			{
				if (_index < _listSheet.Count - 1)
				{
					_index++;
					EditorHelper.StartCoroutine (CreateTable ());
				}
				else
				{
					if (_stringBuilder.ToString () != "")
					{
						EditorHelper.CreateTextFile (Path.GetFileNameWithoutExtension (_dbName), _stringBuilder.ToString (), _dbPath, false, ".sql");
					}
					Debug.Log ("saved name : " + _path);

					EditorUtility.ClearProgressBar ();
					AssetDatabase.Refresh ();
				}
			};

			ISheet sheet = _listSheet [_index];
			if ( sheet.LastRowNum <= 1 )
			{
				innerCreateTable ();
				yield break;
			}

			IRow titleRow = sheet.GetRow (0);
			if ( titleRow == null )
			{
				innerCreateTable ();
				yield break;
			}

			AttributeMappingConfig<ColumnAttribute>[] configs = new AttributeMappingConfig<ColumnAttribute>[titleRow.LastCellNum];
			List<Dictionary<string, object>> list = new List<Dictionary<string, object>> ();
			for ( int i = 1; i <= sheet.LastRowNum; i++ )
			{
				Dictionary<string, object> dic = new Dictionary<string, object> ();
				for ( int j = 0; j < titleRow.LastCellNum; j++ )
				{
					ICell titleCell = titleRow.GetCell (j);
					ICell valueCell = sheet.GetRow (i).GetCell (j);

					Type type = _maker.AddData (titleCell, valueCell, dic);
					if ( type == null )
					{
						continue;
					}

					if ( configs[j] == null )
					{
						configs[j] = new AttributeMappingConfig<ColumnAttribute> ();
						configs[j].name = titleCell.StringCellValue;
						configs[j].type = type;
					}
				}

				if ( dic.Count > 0 )
				{
					list.Add ( dic );
				}
			}

			string dropQuery = _query.GenerateDropTableSQL (sheet.SheetName);
			_stringBuilder.Append ( dropQuery );
			_stringBuilder.AppendLine ();

			string tableQuery = _query.GenerateCreateTableSQL (sheet.SheetName, configs);
			_stringBuilder.Append ( tableQuery );
			_stringBuilder.AppendLine ();

			for ( int i = 0; i < list.Count; i++ )
			{
				string[] column = list [i].Keys.ToArray ();
				string[] value = Array.ConvertAll (list [i].Values.ToArray (), x => x.ToString ());
				string insertQuery = _query.GenerateInsertSQL (sheet.SheetName, column, value);
				_stringBuilder.Append ( insertQuery );
				_stringBuilder.AppendLine ();
			}

			_query.ExecuteNonQuery ( tableQuery );
			_query.INSERT_BATCH ( sheet.SheetName, list );

			yield return null;

			if ( _splitFlag )
			{
				EditorHelper.CreateTextFile ( sheet.SheetName, _stringBuilder.ToString (), _dbPath, false, ".sql" );
				_stringBuilder = new StringBuilder ();
			}

			innerCreateTable ();
		}

		/// <summary>
		/// Create the specified splitFlag and ignores.
		/// </summary>
		/// <param name="splitFlag">If set to <c>true</c> split flag.</param>
		/// <param name="ignores">Ignores.</param>
		public void Create ( bool splitFlag, string[] ignores = null )
		{
			this._splitFlag = splitFlag;

			_maker = new ExcelImportMaker ( _excelFilePath );
			IWorkbook book = _maker.FileStream ();

			if ( book == null )
			{
				return;
			}

			_listSheet = new List<ISheet> ();
			for ( int i = 0; i < book.NumberOfSheets; i++ )
			{
				ISheet sheet = book.GetSheetAt (i);
				if ( Array.IndexOf ( ignores, sheet.SheetName ) < 0 )
				{
					_listSheet.Add ( sheet );
				}
			}

			if ( _listSheet.Count <= 0 )
			{
				return;
			}

			_path = Path.Combine ( _dbPath, _dbName );
			_query = new Query ( _path );
			_query.Sqlite.CreateFile ( true );

			_stringBuilder = new StringBuilder ();

			_index = 0;
			EditorHelper.StartCoroutine ( CreateTable () );
		}
	}
}
