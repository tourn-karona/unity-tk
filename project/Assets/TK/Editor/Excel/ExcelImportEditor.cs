﻿using UnityEditor;
using UnityEngine;
using System;

namespace TK.Editor
{
	public class ExcelImportEditor : EditorWindow
	{
		private const string EXCEL_PATH = "HellgateExcelPath";
		private const string EXCEL_OUTPUT_PATH = "HellgateExcelOutputPath";
		private const string EXCEL_IGNORE_SHEET = "HellgateExcelIgnoreSheet";
		private const string EXCEl_TYPE_SELECTED = "HellgateExcelTypeSelected";
		public static string excelFilePath = "";
		public static string outputJsonPath = "";
		public static string ignoreSheetName = "";
		public static int selected = 0;

		public void OnEnable ()
		{
			excelFilePath = PlayerPrefs.GetString ( EXCEL_PATH );
			ignoreSheetName = PlayerPrefs.GetString ( EXCEL_IGNORE_SHEET );

			string basePath = Application.dataPath + "Excel";
			basePath = basePath.Replace ( "Assets", "" );
			outputJsonPath = PlayerPrefs.GetString ( EXCEL_OUTPUT_PATH, basePath );

			selected = PlayerPrefs.GetInt ( EXCEl_TYPE_SELECTED, 0 );
		}

		public void OnGUI ()
		{
			GUILayout.Label ( "[Excel -> Json] Converter", EditorStyles.boldLabel );
			EditorGUILayout.BeginHorizontal ();
			excelFilePath = EditorGUILayout.TextField ( "Excel file path :", excelFilePath );
			if ( GUILayout.Button ( "...", EditorStyles.miniButtonRight, GUILayout.Width ( 22 ) ) )
			{
				excelFilePath = EditorUtility.OpenFilePanel ( "Where is excel file?", "", "" );
			}
			EditorGUILayout.EndHorizontal ();

			EditorGUILayout.BeginHorizontal ();
			outputJsonPath = EditorGUILayout.TextField ( "Output folder :", outputJsonPath );
			if ( GUILayout.Button ( "...", EditorStyles.miniButtonRight, GUILayout.Width ( 22 ) ) )
			{
				outputJsonPath = EditorUtility.OpenFolderPanel ( "Select your json save folder", "", "" );
			}
			EditorGUILayout.EndHorizontal ();

			selected = EditorGUILayout.Popup ( "Option : ", selected, Enum.GetNames ( typeof ( JsonImportType ) ) );

			GUILayout.Label ( "Set Ignore table name", EditorStyles.boldLabel );
			GUILayout.Label ( "Ex) Info,Version,Description  or  Info|Version|Description" );
			ignoreSheetName = EditorGUILayout.TextField ( "Ignore list :", ignoreSheetName );

			if ( GUILayout.Button ( "Create", GUILayout.Height ( 40 ) ) )
			{
				if ( excelFilePath != "" && outputJsonPath != "" )
				{
					if ( excelFilePath.EndsWith ( "xls" ) || excelFilePath.EndsWith ( "xlsx" ) )
					{
						Create ();
					}
					else
					{
						Debug.LogWarning ( "Please set .xls and .xlsx file." );
					}
				}
			}
		}

		[MenuItem ( "Window/TK/Json Converter for Excel", false, 11 )]
		public static void ShowWindow ()
		{
			EditorWindow.GetWindow ( typeof ( ExcelImportEditor ) );
		}

		/// <summary>
		/// Create the specified extension.
		/// </summary>
		/// <param name="extension">Extension.</param>
		public static void Create ()
		{
			ExcelImportMaker maker = new ExcelImportMaker (excelFilePath, outputJsonPath);

			string[] ignores = ignoreSheetName.Split (new string[] { ",", "|" }, StringSplitOptions.None);
			if ( selected == 0 )
			{
				maker.Create ( JsonImportType.NORMAL, ignores );
			}
			else
			{
				maker.Create ( JsonImportType.ATTRIBUTE, ignores );
			}

			PlayerPrefs.SetString ( EXCEL_PATH, excelFilePath );
			PlayerPrefs.SetString ( EXCEL_IGNORE_SHEET, ignoreSheetName );
			PlayerPrefs.SetString ( EXCEL_OUTPUT_PATH, outputJsonPath );
			PlayerPrefs.SetInt ( EXCEl_TYPE_SELECTED, selected );
		}
	}
}
