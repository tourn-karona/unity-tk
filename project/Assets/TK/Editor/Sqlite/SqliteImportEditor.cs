﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace TK.Editor
{
    public class SqliteImportEditor : EditorWindow
    {
        private const string DB_PATH = "DBPath";
        private const string DB_OUTPUT_PATH = "DBOutputPath";
        private const string DB_IGNORE_TABLE = "DBIgnoreTable";
        public static string sqlitePath;
        public static string outputJsonPath;
        public static string ignoreTableName;
        public static Query query;

        public void OnEnable ()
        {
            sqlitePath = PlayerPrefs.GetString (DB_PATH);
            ignoreTableName = PlayerPrefs.GetString (DB_IGNORE_TABLE);

            string basePath = Application.dataPath + "Sqlite";
            basePath = basePath.Replace ("Assets", "");
            outputJsonPath = PlayerPrefs.GetString (DB_OUTPUT_PATH, basePath);
        }

        public void OnGUI ()
        {
            GUILayout.Label ("[Sqlite DB -> Json] Converter", EditorStyles.boldLabel);
            EditorGUILayout.BeginHorizontal ();
            sqlitePath = EditorGUILayout.TextField ("Sqlite DB file path :", sqlitePath);
            if (GUILayout.Button ("...", EditorStyles.miniButtonRight, GUILayout.Width (22))) {
                sqlitePath = EditorUtility.OpenFilePanel ("Where is DB file?", "", "");
            }
            EditorGUILayout.EndHorizontal ();

            EditorGUILayout.BeginHorizontal ();
            outputJsonPath = EditorGUILayout.TextField ("Output folder :", outputJsonPath);
            if (GUILayout.Button ("...", EditorStyles.miniButtonRight, GUILayout.Width (22))) {
                outputJsonPath = EditorUtility.OpenFolderPanel ("Select your json save folder", "", "");
            }
            EditorGUILayout.EndHorizontal ();

            GUILayout.Label ("Set Ignore DB table name", EditorStyles.boldLabel);
            GUILayout.Label ("Ex) Info,Version,Description  or  Info|Version|Description");
            ignoreTableName = EditorGUILayout.TextField ("Ignore list :", ignoreTableName);

            if (GUILayout.Button ("Create", GUILayout.Height (40))) {
                if (sqlitePath != "" && outputJsonPath != "") {
                    Create ();
                }
            }
        }

        [MenuItem ("Window/TK/Json Converter for Sqlite DB", false, 11)]
        public static void ShowWindow ()
        {
            EditorWindow.GetWindow (typeof(SqliteImportEditor));
        }

        /// <summary>
        /// Create this instance.
        /// </summary>
        public static void Create ()
        {
            // save
            PlayerPrefs.SetString (DB_PATH, sqlitePath);
            PlayerPrefs.SetString (DB_OUTPUT_PATH, outputJsonPath);
            PlayerPrefs.SetString (DB_IGNORE_TABLE, ignoreTableName);

            string[] ignores = ignoreTableName.Split (new string[] { ",", "|" }, System.StringSplitOptions.None);

            query = new Query (sqlitePath);

            SqliteMastser[] master = query.SELECT<SqliteMastser> ("type", (object)"table");
            if (master == null) {
                Debug.Log ("There are no tables");
            } else {
                for (int i = 0; i < master.Length; i++) {
                    if (master [i].Name == "sqlite_sequence") {
                        continue;
                    }

                    if (Array.IndexOf (ignores, master [i].Name) >= 0) {
                        continue;
                    }

                    CreateUsalJson (master [i].Name);
                }
            }

        }

        /// <summary>
        /// Creates the usal json.
        /// </summary>
        /// <param name="tableName">Table name.</param>
        public static void CreateUsalJson (string tableName)
        {
            DataTable data = query.SELECT (tableName);
            if (data.Rows.Count <= 0) {
                return;
            }

            string json = JsonUtil.ToJson (data.Rows);
            EditorHelper.CreateJsonFile (tableName, json, outputJsonPath);
        }
    }
}

