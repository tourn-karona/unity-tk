﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Data;
using Mono.Data.Sqlite;

namespace TK.Editor
{
    public class SqliteEditor : EditorWindow
    {
        private const string DB_NAME = "DBName";
        public static string dbName = "";

        public void OnEnable ()
        {
            dbName = PlayerPrefs.GetString (DB_NAME);
        }

        public void OnGUI ()
        {
            GUILayout.Label ("Create Sqlite DB", EditorStyles.boldLabel);

            dbName = EditorGUILayout.TextField ("Sqlite DB name :", dbName);

            if (GUILayout.Button ("Create", GUILayout.Height (40))) {
                if (dbName != "") {
                    Create ();
                }
            }
        }

        [MenuItem ("Window/TK/Create Sqlite DB", false, 13)]
        public static void ShowWindow ()
        {
            EditorWindow.GetWindow (typeof(SqliteEditor));
        }

        public static void Create ()
        {
            Sqlite sqlite = new Sqlite ();
            sqlite.AutoDDL (dbName, true);
            Debug.Log ("StreamingAssets Create Sqlite DB : " + dbName);
        }
    }
}
