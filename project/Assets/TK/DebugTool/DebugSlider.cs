﻿using UnityEngine;

namespace TK.DebugTool
{
	public class DebugSlider : DebugItem
	{
		private DebugFieldAccessor<float> _accessor = null;
		private float _left = 0f;
		private float _right = 0f;
		private float _value = 0f;

		public DebugSlider ( string name, DebugFieldAccessor<float> accessor, float left, float right ) : base ( name )
		{
			_accessor = accessor;
			_left = left;
			_right = right;
		}

		public override void Draw ()
		{
			_value = _accessor.Value;
			GUILayout.BeginVertical (Menu.Style.BoxStyle, GUILayout.MaxWidth ( Menu.MaxDebugItemWidth ) );
			GUILayout.Label ( NameContent );
			GUILayout.Label ( _value.ToString () );
			_accessor.Value = GUILayout.HorizontalSlider ( _value, _left, _right, GUILayout.ExpandWidth(true) );
			GUILayout.EndVertical ();
		}
	}
}
