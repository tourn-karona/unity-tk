namespace TK.DebugTool
{
	public class DebugMenu
	{
		private IDebugGroup _rootItem = null;
		private IDebugGroup _curItem = null;
		public float maxHeight = 0f;
		public float maxWidth = 0f;

		public float MaxDebugItemWidth
		{
			get { return maxWidth * DebugStyle.DEBUG_ITEM_WIDTH_PERCENT; }
		}

		public IDebugGroup Root
		{
			get { return _rootItem; }
		}

		public IDebugGroup CurrentItem
		{
			get { return _curItem; }
			set { _curItem = value; }
		}

		public bool IsDebugOpening
		{
			get { return _curItem.IsAccess; }
		}

		public DebugStyle Style { get; private set; }

		public DebugMenu()
		{
			_rootItem = new DebugGroup("Debug");
			_rootItem.Menu = this;
			_curItem = _rootItem;

			Style = new DebugStyle ();
		}

		public void Close()
		{
			_curItem.Close();
		}

		// Update is called once per frame
		public void Draw()
		{
			_curItem.Draw();
		}
	}

}
