using System.Reflection;

namespace TK.DebugTool
{

	public class DebugFieldAccessor<ValueType>
	{

		private object _obj = null;
		private FieldInfo _field = null;

		public ValueType Value
		{
			get { return (ValueType)_field.GetValue(_obj); }
			set { _field.SetValue(_obj, value); }
		}

		private DebugFieldAccessor()
		{
		}

		public static DebugFieldAccessor<ValueType> Bind(object obj, string fieldName)
		{

			DebugFieldAccessor<ValueType> accessor = new DebugFieldAccessor<ValueType> ();
			FieldInfo field = obj.GetType ().GetField (fieldName, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance);
			accessor._obj = obj;
			accessor._field = field;
			return accessor;

		}

	}

}
