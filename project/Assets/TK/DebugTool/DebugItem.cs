using UnityEngine;

namespace TK.DebugTool
{

	public abstract class DebugItem : IDebugItem
	{
		private string _name = "";
		private GUIContent _content = new GUIContent ();

		public virtual string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
				_content.text = value;
			}
		}

		public object Tag { get; set; }

		public DebugMenu Menu { get; set; }

		protected virtual GUIContent NameContent
		{
			get
			{
				return _content;
			}
		}

		public DebugItem (string name)
		{
			Name = name;
		}

		public abstract void Draw ();
	}

}
