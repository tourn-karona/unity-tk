using UnityEngine;
using UnityEngine.Events;

namespace TK.DebugTool
{

	public class DebugInput : DebugItem
	{
		private string _value = "";
		private UnityAction<DebugInput, string> _act = null;
		private float _width = 0f;
		private string _buttonName = null;

		public DebugInput(string name, UnityAction<DebugInput, string> action, string buttonName = "Submit") : base(name)
		{
			_act = action;
			_buttonName = buttonName;
		}

		public override void Draw ()
		{
			_width = Menu.MaxDebugItemWidth;
			GUILayout.BeginVertical ( Menu.Style.BoxStyle, GUILayout.MaxWidth ( _width ) );
			GUILayout.Label ( NameContent );
			GUILayout.BeginHorizontal ( GUILayout.MaxWidth ( _width ) );
			_value = GUILayout.TextField ( _value, GUILayout.MaxWidth ( _width * 0.65f ) );
			if ( GUILayout.Button ( _buttonName, GUILayout.MaxWidth ( _width * 0.3f ) ) )
			{
				if ( _act != null )
				{
					_act ( this, _value );
				}
			}
			GUILayout.EndHorizontal ();
			GUILayout.EndVertical();
		}
	}

}
