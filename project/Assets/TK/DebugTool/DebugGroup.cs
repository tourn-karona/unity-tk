// Unity name space
using UnityEngine;

// Microsoft name space
using System;
using System.Collections.Generic;

namespace TK.DebugTool
{

	/// <summary>
	/// Debug group to list all debug items.
	/// </summary>
	public class DebugGroup : DebugCollection<DebugGroup>
	{
		private const string LABEL_BACK = "Back";
		private const string LABEL_CLOSE = "Close";
		private Vector2 _scrollPos = Vector2.zero;
		private GUIStyle _scrollStyle = null;
		private int _itemCount = 0;

		// List to store all items
		private List<IDebugItem> items = new List<IDebugItem> ();

		public DebugGroup (string name) : base (name)
		{
		}

		private bool IsDuplicated (IDebugItem item)
		{
			return items.Exists (e => e.Name == item.Name);
		}

		/// <summary>
		/// Add the specified debug item.
		/// </summary>
		/// <param name="item">New item.</param>
		public override IDebugGroup Add (IDebugItem item)
		{
            if (item == null)
            {
				throw new InvalidOperationException ( "Debug item cannot be null." );
            }

			if (IsDuplicated (item))
			{
				throw new InvalidOperationException ( string.Format ( "Debug item {0} already exists.", item.Name ) );
			}

			if (item is IDebugGroup)
			{
				((IDebugGroup)item).Parent = this;
			}

			item.Menu = Menu;
			items.Add (item);
			_itemCount++;

			return this;
		}

		/// <summary>
		/// Remove debug items by the specified name.
		/// </summary>
		/// <param name="name">Name of item to be deleted. Name can be a single name or path separated by forwardslash (/).</param>
		public override void Remove (string name)
		{
			int index = name.IndexOf ('/');

			if (index >= 0)
			{
				string parentName = name.Substring (0, index);

				IDebugGroup group = (IDebugGroup)items.Find (item => item.Name == parentName);
				if (group != null)
				{
					group.Remove (name.Substring (index + 1));
				}
			}
			else
			{
				items.RemoveAll (item => item.Name == name);
				_itemCount = items.Count;
			}
		}

		public override void Clear ()
		{
			_itemCount = 0;
			items.Clear ();
		}

		public override void Close ()
		{
			if (Menu.CurrentItem == this)
			{
				//InvokeOnExit ();
				Back ();
				if (Parent != null && Parent is IDebugGroup)
				{
					((IDebugGroup)Parent).Close ();
				}
			}
			else
			{
				Menu.CurrentItem.Close ();
			}
		}

		public override void Back ()
		{
			InvokeOnExit ();
			if (Parent != null && Parent is IDebugGroup)
			{
				Parent.InvokeOnAccess ();
				Menu.CurrentItem = Parent;
			}
		}

		/// <summary>
		/// Draws the item list.
		/// </summary>
		protected override void DrawContent ()
		{
			GUILayout.BeginVertical (Menu.Style.BoxStyle, GUILayout.MaxWidth(Menu.maxWidth), GUILayout.MaxHeight(Menu.maxHeight));

			GUILayout.Label (TitleContent);

			GUILayout.BeginHorizontal ();

			// TODO: Draw button back
			if (GUILayout.Button (LABEL_BACK))
			{
				Back ();
			}

			// TODO: Draw button close
			if (GUILayout.Button (LABEL_CLOSE))
			{
				Close ();
			}

			GUILayout.EndHorizontal ();

			if (_scrollStyle == null)
			{
				_scrollStyle = new GUIStyle (GUI.skin.scrollView);

				// Set padding right for 20 px to avoid overlapping
				// the right scrollbar over the elements
				_scrollStyle.padding.right = 20;
			}

			_scrollPos = GUILayout.BeginScrollView ( _scrollPos, _scrollStyle, GUILayout.MaxWidth ( Menu.maxWidth ) );
			DrawItemList ();
			GUILayout.EndScrollView ();

			GUILayout.EndVertical ();
		}

		private void DrawItemList ()
		{
			// Draw to list all debug items
			for ( int i = 0; i < _itemCount; i++ )
			{
				IDebugItem item = items [i];
				item.Draw ();
				if ( item is IDebugGroup )
				{
					IDebugGroup group = (IDebugGroup)item;
					if ( group.IsAccess )
					{
						InvokeOnExit ();
						Menu.CurrentItem = group;
						break;
					}
				}
			}
		}

	}

}
