﻿using UnityEngine;

namespace TK.DebugTool
{
	public class DebugString : DebugItem
	{
		private DebugFieldAccessor<string> _accessor = null;
		private float _width = 0f;

		public DebugString ( string name, DebugFieldAccessor<string> accessor ) : base ( name )
		{
			this._accessor = accessor;
		}

		public override void Draw ()
		{
			_width = Menu.MaxDebugItemWidth;
			GUILayout.BeginVertical ( Menu.Style.BoxStyle, GUILayout.MaxWidth ( _width ) );
			GUILayout.Label ( Name );
			_accessor.Value = GUILayout.TextField ( _accessor.Value, GUILayout.MaxWidth ( _width * 0.95f ) );
			GUILayout.EndVertical ();
		}

	}
}
