﻿using UnityEngine;

namespace TK.DebugTool
{
	public class DebugStyle
	{
		public const float DEBUG_ITEM_OFFSET = 40f;
		public const float DEBUG_ITEM_WIDTH_PERCENT = 0.85f;

		private GUIStyle _boxStyle = null;
		private GUIStyle _buttonStyle = null;

		public GUIStyle ButtonStyle
		{
			get
			{
				if ( _buttonStyle == null )
				{
					_buttonStyle = new GUIStyle ( GUI.skin.button );
					_buttonStyle.alignment = TextAnchor.MiddleLeft;
				}
				return _buttonStyle;
			}
		}

		public GUIStyle BoxStyle
		{
			get
			{
				if ( _boxStyle == null )
				{
					Texture2D tex = GenerateStyleTexture ( "#2f3542c8".HexToColor (), Color.black );
					_boxStyle = new GUIStyle ( GUI.skin.box );
					_boxStyle.normal.background = tex;
					_boxStyle.hover.background = tex;
					_boxStyle.active.background = tex;
				}
				return _boxStyle;
			}
		}

		public static Texture2D GenerateStyleTexture ( Color fillColor, Color borderColor )
		{
			// Generate background texture for the box style of the debug group
			int pixels = 12;
			Texture2D texture = new Texture2D (pixels, pixels);
			for ( int y = 0; y < pixels; y++ )
			{
				for ( int x = 0; x < pixels; x++ )
				{
					if ( x == 0 || y == 0 || x == (pixels - 1) || y == (pixels - 1) )
					{
						texture.SetPixel ( x, y, borderColor );
					}
					else
					{
						texture.SetPixel ( x, y, fillColor );
					}
				}
			}
			texture.Apply ();
			return texture;
		}
	}
}