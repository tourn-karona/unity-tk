using UnityEngine;
using UnityEngine.Events;

namespace TK.DebugTool
{
	// Draw action button debug
	public class DebugAction : DebugItem
	{
		private UnityAction _act = null;

		public DebugAction(string name, UnityAction action) : base(name)
		{
			_act = action;
		}

		public DebugAction(string name, UnityAction<DebugAction> paramAction) : base(name)
		{
			if ( paramAction == null )
			{
				return;
			}
			_act = () => paramAction ( this );
		}

		public override void Draw()
		{
			if ( GUILayout.Button ( NameContent, Menu.Style.ButtonStyle, GUILayout.Width ( Menu.maxWidth - DebugStyle.DEBUG_ITEM_OFFSET ) ) == false )
			{
				return;
			}

			if ( _act == null )
			{
				return;
			}

			_act ();
		}
	}
}
