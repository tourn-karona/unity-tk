using System.Collections.Generic;
using UnityEngine;

namespace TK.DebugTool
{
    public class DebugLogContent : DebugItem
    {
        public class LogMessage
        {
            private string _condition = "";
            private string _stackTrace = "";
            private string _title = "";
            private GUIStyle _style = null;
            private bool _hasStackTrace = false;

            private string GetRichTextLog(string text, LogType type)
            {
                string log = "";
				if ( type == LogType.Error || type == LogType.Assert )
				{
					log += "<color=red>";
				}
				else if ( type == LogType.Warning )
				{
					log += "<color=yellow>";
				}
				else
				{
					log += "<color=white>";
				}
                log += text;
                log += "</color>";
                return log;
            }

            private string GetBoldRichText(string text)
            {
                return string.Format("<b>{0}</b>", text);
            }

            public LogMessage(string title, string condition, string stackTrace, LogType type)
            {
                _title = GetBoldRichText(GetRichTextLog(title, type));
                _condition = GetRichTextLog(condition, type);

                _hasStackTrace = !string.IsNullOrEmpty(stackTrace);
                if (_hasStackTrace)
                {
                    _stackTrace = GetRichTextLog(stackTrace, type);
                }
            }

            public void Draw()
            {

                if (_style == null)
                {
                    _style = new GUIStyle(GUI.skin.box);
                    _style.richText = true;
                    _style.wordWrap = true;
                    _style.alignment = TextAnchor.UpperLeft;
                }

                if (_hasStackTrace)
                {
                    GUILayout.Label(string.Format("{0}\n{1}\n{2}", _title, _condition, _stackTrace), _style);
                }
                else
                {
                    GUILayout.Label(string.Format("{0}\n{1}", _title, _condition), _style);
                }
            }
        }

        private List<LogMessage> _logs = new List<LogMessage>();
		private int _logCount = 0;
        private bool _showStackTrace = false;
        private GUIStyle _style = null;

        public DebugLogContent(string name, bool showStackTrace, string predefinedLog = "") : base(name)
        {
            _logs.Add(new LogMessage(System.DateTime.Now.ToShortDateString() + " - " + System.DateTime.Now.ToShortTimeString(), predefinedLog, "", LogType.Log));
			_logCount++;
        }

        public void Prepare()
        {
            Application.logMessageReceivedThreaded += Application_logMessageReceived;
        }

        public void Release()
        {
            Application.logMessageReceivedThreaded -= Application_logMessageReceived;
        }

        private void Application_logMessageReceived(string condition, string stackTrace, LogType type)
        {
            string logTime = System.DateTime.Now.ToShortDateString() + " - " + System.DateTime.Now.ToShortTimeString();

            if (_showStackTrace)
            {
                _logs.Add(new LogMessage(logTime, condition, stackTrace, type));
            }
            else
            {
                _logs.Add(new LogMessage(logTime, condition, "", type));
            }

			_logCount++;
		}

        public override void Draw()
        {
            if (_style == null)
            {
                _style = new GUIStyle(GUI.skin.textArea);
                _style.richText = true;
            }

			GUILayout.BeginVertical ( GUILayout.MaxWidth ( Menu.MaxDebugItemWidth ) );

            if (GUILayout.Button("Clear"))
            {
                _logs.Clear();
				_logCount = 0;
            }

			for ( int i = 0; i < _logCount; i++ )
			{
				_logs[i].Draw ();
			}

			GUILayout.EndVertical();
        }
    }

    public class DebugLog : DebugGroup
    {
        private DebugLogContent _logContent = null;

        public DebugLog(string name, bool showStackTrace, string predefinedLog = "") : base(name)
        {
            OnAccess = d =>
            {
                _logContent = new DebugLogContent("Log", showStackTrace, predefinedLog);
                Add(_logContent);
                _logContent.Prepare();
            };

            OnExit = d =>
            {
                _logContent.Release();
                Remove("Log");
            };
        }

        public DebugLog(string name, bool showStackTrace, System.Func<string> getPredefinedLog) : base(name)
        {
            OnAccess = d =>
            {
                _logContent = new DebugLogContent("Log", showStackTrace, getPredefinedLog());
                Add(_logContent);
                _logContent.Prepare();
            };

            OnExit = d =>
            {
                _logContent.Release();
                Remove("Log");
            };
        }
    }
}
