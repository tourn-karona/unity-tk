﻿using UnityEngine;

namespace TK.DebugTool
{
	public class DebugFloat : DebugItem
	{
		private DebugFieldAccessor<float> _accessor = null;
		private float _changeValue = 0f;
		private float _width = 0f;

		public DebugFloat ( string name, DebugFieldAccessor<float> accessor, float changeValue ) : base ( name )
		{
			_accessor = accessor;
			_changeValue = changeValue;
		}

		public override void Draw ()
		{
			_width = Menu.MaxDebugItemWidth;
			GUILayout.BeginVertical ( Menu.Style.BoxStyle, GUILayout.MaxWidth ( _width ) );
			GUILayout.Label ( Name );
			GUILayout.BeginHorizontal ( GUILayout.MaxWidth ( _width ) );
			if ( GUILayout.Button ( " - ", GUILayout.Width ( _width * 0.2f ) ) )
			{
				_accessor.Value -= _changeValue;
			}
			GUILayout.Label ( _accessor.Value.ToString () );
			if ( GUILayout.Button ( " + ", GUILayout.Width ( _width * 0.2f ) ) )
			{
				_accessor.Value += _changeValue;
			}
			GUILayout.EndHorizontal ();
			GUILayout.EndVertical ();
		}
	}
}