using UnityEngine;
using UnityEngine.Events;
namespace TK.DebugTool
{

	public abstract class DebugCollection<T> : DebugItem, IDebugGroup where T : DebugCollection<T>
	{
		public UnityAction<T> OnAccess = null;
		public UnityAction<T> OnExit = null;
		private GUIContent _titleContent = new GUIContent ();

		public bool IsAccess { get; set; }

		// Get or set parent group that convers this debug item
		// The parent must be the group
		public IDebugGroup Parent { get; set; }

		public override string Name
		{
			get
			{
				return base.Name;
			}
			set
			{
				base.Name = value;
				_titleContent.text = value;
				NameContent.text = "[+] " + value;
			}
		}

		public DebugCollection(string name) : base(name)
		{
		}

		protected GUIContent TitleContent
		{
			get
			{
				return _titleContent;
			}
		}

		public void InvokeOnAccess()
		{
			IsAccess = true;
			if (OnAccess != null)
			{
				OnAccess((T)this);
			}
		}

		public void InvokeOnExit()
		{
			IsAccess = false;
			if (OnExit != null)
			{
				OnExit((T)this);
			}
		}

		/// <summary>
		/// Draws the main button of the group.
		/// </summary>
		protected void DrawMainButton()
		{
			if (GUILayout.Button(NameContent, Menu.Style.ButtonStyle) )
			{
				InvokeOnAccess();
			}
		}

		public override void Draw()
		{
			if (IsAccess)
			{
				DrawContent();
			}
			else
			{
				DrawMainButton();
			}
		}

		public abstract IDebugGroup Add(IDebugItem item);
		public abstract void Remove (string name);
		public abstract void Close();
		public abstract void Back();
		public abstract void Clear();
		protected abstract void DrawContent();
	}

}
