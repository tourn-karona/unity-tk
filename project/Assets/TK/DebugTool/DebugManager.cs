using UnityEngine;

namespace TK.DebugTool
{

	public class DebugManager : MonoSingleton<DebugManager>
	{
		private const float BASE_WIDTH = 320f;  // define here the original resolution
		private const float FIT_WIDTH_OFFSET = 8f;
		private const float WIDTH_PERCENT = 0.8f;
		private float _sw = 0f;
		private float _sh = 0f;
		private float _bw = 0f;
		private float _bh = 0f;
		private Matrix4x4 _svMat;
		private Vector3 _guiscale = new Vector3(1f, 1f, 1f);
		private Matrix4x4 _newMatrix = new Matrix4x4();
		private float _ratio = 0f;
		private bool _isLandscape = false;
		private DebugMenu _menu = new DebugMenu();

		private void OnGUI()
        {
            _svMat = GUI.matrix; // save current matrix
								 // substitute matrix - only scale is altered from standard

			_sw = Screen.width;
			_sh = Screen.height;

			_isLandscape = (_sw > _sh);

			if ( _isLandscape )
			{
				_ratio = ScriptUtility.CalAspectRatio ( _sh, _sw );
			}
			else
			{
				_ratio = ScriptUtility.CalAspectRatio ( _sw, _sh );
			}

			_bw = BASE_WIDTH;
			_bh = ScriptUtility.CalHeightByAspectRatio(_bw, _ratio);

			if ( _isLandscape )
			{
				_guiscale.x = _sw / _bh;
				_guiscale.y = _sh / _bw;

				_menu.maxHeight = _bw;
				_menu.maxWidth = (_bh - FIT_WIDTH_OFFSET) * WIDTH_PERCENT;
			}
			else
			{
				_guiscale.x = _sw / _bw;
				_guiscale.y = _sh / _bh;

				_menu.maxHeight = _bh;
				_menu.maxWidth = (_bw - FIT_WIDTH_OFFSET) * WIDTH_PERCENT;
			}

			_newMatrix.SetTRS(Vector3.zero, Quaternion.identity, _guiscale);

            GUI.matrix = _newMatrix;

            // Draw debug menu
            _menu.Draw();

            // restore matrix before returning
            GUI.matrix = _svMat; // restore matrix
        }

		public override bool DestroyOnLoad
		{
			get
			{
				return false;
			}
		}

        public IDebugGroup Root
        {
            get { return _menu.Root; }
        }

        public bool IsDebugOpening
        {
            get { return _menu.IsDebugOpening; }
        }

        public void Close()
        {
            _menu.Close();
        }
	}

}
