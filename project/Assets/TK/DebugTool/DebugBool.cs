using UnityEngine;
using UnityEngine.Events;

namespace TK.DebugTool
{
	/// <summary>
	/// Debug boolean value
	/// </summary>
	public class DebugBool : DebugItem
	{
		public UnityAction<bool> onValueChanged = null;
		private bool _currentValue = false;
		private bool _newValue = false;

		public DebugBool(string name, bool currentValue, UnityAction<bool> onValueChanged) : base(name)
		{
			_currentValue = currentValue;
			this.onValueChanged = onValueChanged;
		}

		public DebugBool ( string name, DebugFieldAccessor<bool> accessor ) : this ( name, accessor.Value, ( value ) => accessor.Value = value )
		{
		}

		public override void Draw()
		{
			_newValue = GUILayout.Toggle ( _currentValue, NameContent, GUILayout.MaxWidth ( Menu.MaxDebugItemWidth ) );

			if ( _newValue == _currentValue)
			{
				return;
			}

			_currentValue = _newValue;

			if ( onValueChanged == null )
			{
				return;
			}

			onValueChanged ( _currentValue );
		}
	}
}
