using System.Collections.Generic;

namespace TK.DebugTool
{

	public class DebugBoolArray : DebugGroup
	{

		private bool[] _array = null;

		public DebugBoolArray(string name, bool[] array) : base(name)
		{
			_array = array;
			OnAccess = OnAccessCallback;
			OnExit = OnExitCallback;
		}

		private void OnAccessCallback(DebugGroup group)
		{
			int length = _array.Length;
			for (int i = 0; i < length; i++)
			{
				int index = i;
				Add(new DebugBool(index.ToString(), _array[index], (value) => _array[index] = value));
			}
		}

		private void OnExitCallback(DebugGroup group)
		{
			Clear();
		}

	}

	public class DebugBoolList : DebugGroup
	{

		private List<bool> _list = null;

		public DebugBoolList(string name, List<bool> list) : base(name)
		{
			_list = list;
			OnAccess = OnAccessCallback;
			OnExit = OnExitCallback;
		}

		private void OnAccessCallback(DebugGroup group)
		{
			int length = _list.Count;
			for (int i = 0; i < length; i++)
			{
				int index = i;
				Add(new DebugBool(index.ToString(), _list[index], (value) => _list[index] = value));
			}
		}

		private void OnExitCallback(DebugGroup group)
		{
			Clear();
		}

	}

}
