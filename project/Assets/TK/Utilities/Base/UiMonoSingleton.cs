using UnityEngine;
using UnityEngine.EventSystems;

namespace TK
{
	public abstract class UiMonoSingleton<T> : UIBehaviour where T : UiMonoSingleton<T>
	{
		private static T _instance = null;
		private Transform _transform = null;
		private RectTransform _rectTransform = null;

		public Transform Transform
		{
			get
			{
				if ( _transform == null )
				{
					_transform = transform;
				}
				return _transform;
			}
		}

		public RectTransform RectTransform
		{
			get
			{
				if ( _rectTransform == null )
				{
					if ( transform is RectTransform )
					{
						_rectTransform = transform as RectTransform;
					}
				}
				return _rectTransform;
			}
		}

		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<T> ();
					if (_instance == null)
					{
						new GameObject ('_' + typeof (T).Name, typeof (T));
					}
				}
				return _instance;
			}
		}

		public abstract bool DestroyOnLoad { get; }

		protected override void Awake ()
		{
			if (_instance == null)
			{
				_instance = this as T;
				if (DestroyOnLoad == false)
				{
					DontDestroyOnLoad (gameObject);
				}
			}
			base.Awake ();
		}

		protected override void Start ()
		{
			if (_instance != null && !_instance.Equals (this))
			{
				Destroy (gameObject);
				return;
			}
			base.Start ();
		}
	}

}
