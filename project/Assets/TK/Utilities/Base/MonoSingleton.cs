using UnityEngine;

namespace TK
{
	public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
	{
		private static T _instance = null;
		private Transform _transform = null;
		private RectTransform _rectTransform = null;

		public Transform Transform
		{
			get
			{
				if ( _transform == null )
				{
					_transform = transform;
				}
				return _transform;
			}
		}

		public RectTransform RectTransform
		{
			get
			{
				if ( _rectTransform == null )
				{
					if ( transform is RectTransform )
					{
						_rectTransform = transform as RectTransform;
					}
				}
				return _rectTransform;
			}
		}

		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = GameObject.FindObjectOfType<T> ();
					if (_instance == null)
					{
						_instance = new GameObject ('_' + typeof (T).Name, typeof (T)).GetComponent<T> ();
					}
				}
				return _instance;
			}
		}

		public abstract bool DestroyOnLoad { get; }

		protected virtual void Awake ()
		{
			if (_instance == null)
			{
				_instance = this as T;
				if (DestroyOnLoad == false)
				{
					DontDestroyOnLoad (gameObject);
				}
			}
		}

		protected virtual void Start ()
		{
			if (_instance != null && !_instance.Equals (this))
			{
				Destroy (gameObject);
				return;
			}
		}
	}

}
