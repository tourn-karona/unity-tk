﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace TK.UI
{

    public class StableClickTrigger : MonoBehaviour,
    IBeginDragHandler,
    IEndDragHandler,
    IDragHandler,
    IPointerDownHandler,
    IPointerUpHandler,
    IPointerExitHandler
    {
        public interface Listener
        {
            StableClickTrigger Trigger { get; set; }
            void OnPointerDown(PointerEventData eventData);
            void OnPointerUp(PointerEventData eventData);
            void OnPointerClick(PointerEventData eventData);
        }

        [SerializeField]
        private float _maxDistance = 10;
        private Vector2 _startPos;

        private Listener _pickedListener = null;

        private bool IsLeftPointerButton(PointerEventData eventData)
        {
            return eventData.button == PointerEventData.InputButton.Left;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if (!IsLeftPointerButton(eventData))
            {
                return;
            }

            _startPos = eventData.position;
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!IsLeftPointerButton(eventData))
            {
                return;
            }

            float distance = Vector2.Distance(_startPos, eventData.position);

            if (distance >= _maxDistance)
            {
                _pickedListener = null;
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (!IsLeftPointerButton(eventData))
            {
                return;
            }

            NotifyClick(eventData);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!IsLeftPointerButton(eventData))
            {
                return;
            }

            _pickedListener = null;

            if (eventData.pointerPressRaycast.gameObject)
            {
                _pickedListener = eventData.pointerPressRaycast.gameObject.GetComponent<Listener>();
            }

            if (_pickedListener != null)
            {
                _pickedListener.Trigger = this;
                _pickedListener.OnPointerDown(eventData);
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!IsLeftPointerButton(eventData))
            {
                return;
            }

            NotifyClick(eventData);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!IsLeftPointerButton(eventData))
            {
                return;
            }

            if (_pickedListener != null)
            {
                _pickedListener.OnPointerUp(eventData);
                _pickedListener = null;
            }
        }

        private void NotifyClick(PointerEventData eventData)
        {
            if (_pickedListener == null)
            {
                return;
            }

            if (_pickedListener != null)
            {
                _pickedListener.OnPointerUp(eventData);
                _pickedListener.OnPointerClick(eventData);
                _pickedListener = null;
            }
        }
    }
}