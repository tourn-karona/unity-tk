﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace TK.UI
{

    public class StableClickListener : MonoBehaviour, StableClickTrigger.Listener, IPointerExitHandler
    {
        public UnityEvent onPointerDown = new UnityEvent();
        public UnityEvent onPointerUp = new UnityEvent();
        public UnityEvent onClick = new UnityEvent();

        public StableClickTrigger Trigger { get; set; }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            onClick.Invoke();
        }

        public virtual void OnPointerDown(PointerEventData eventData)
        {
            onPointerDown.Invoke();
        }

        public virtual void OnPointerUp(PointerEventData eventData)
        {
            onPointerUp.Invoke();
        }

        void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
        {
            if (Trigger != null)
            {
                Trigger.OnPointerExit(eventData);
            }
        }
    }
}