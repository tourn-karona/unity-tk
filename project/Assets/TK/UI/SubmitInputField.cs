﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class SubmitInputField : InputField
{
	[Serializable]
	public class OnSubmitEvent : UnityEvent<string>{}

	public OnSubmitEvent onSubmit = new OnSubmitEvent();

	protected override void Awake ()
	{
		base.Awake ();

		#if UNITY_EDITOR || UNITY_STANDALONE
		onEndEdit.AddListener (OnEndEdit);
		#endif
	}

	#if UNITY_EDITOR || UNITY_STANDALONE
	private void OnEndEdit(string text) {

		if (Input.GetKeyDown (KeyCode.Return) || Input.GetKeyDown (KeyCode.KeypadEnter)) {
		
			onSubmit.Invoke (text);

		}

	}
	#endif

	private void Update ()
	{
		#if !UNITY_EDITOR && (UNITY_ANDROID || UNITY_IOS)
		if (m_Keyboard != null && m_Keyboard.done && !m_Keyboard.wasCanceled) {
			onSubmit.Invoke (text);
		}
		#endif
	}
}