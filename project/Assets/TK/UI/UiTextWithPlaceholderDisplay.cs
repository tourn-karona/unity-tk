﻿using UnityEngine;
using UnityEngine.UI;

namespace TK.UI
{
    [ExecuteInEditMode]
    public class UiTextWithPlaceholderDisplay : MonoBehaviour
    {
        [SerializeField]
        private Graphic _placeholder = null;

        [SerializeField]
        private Text _textComponent = null;

        public string text
        {
            get
            {
                return _textComponent.text;
            }
            set
            {
                bool empty = value.IsNullOrEmpty();

#if UNITY_EDITOR
                _textValue = value;
#endif

                if (_textComponent)
                {
                    _textComponent.text = value;
                }

                if (_placeholder)
                {
                    _placeholder.enabled = empty;
                }
            }
        }

#if UNITY_EDITOR
        public string _textValue = "";

        // Update is called once per frame
        void Update()
        {
            if (!Application.isPlaying)
            {
                text = _textValue;
            }
        }
#endif
    }

}